export default class NamingService {
    public static generateFolderName(url: string): string {

        let folderName = url;

        if (url.indexOf('.git') >= 0) {
            folderName = this.cutGit(folderName);
        }
        folderName = this.getProjectTitle(folderName);

        return folderName;
    }

    private static cutGit(url: string): string {
        url = url.slice(0, url.indexOf('.git'));
        return url;
    }

    public static getPushRefSpecs(branchRefName: string): string[] {
        const branchName = this.getBranchName(branchRefName)
        return ['refs/heads/' + branchName + ':refs/heads/' + branchName];
    }

    public static getBranchName(branchRefName: string): string {
        return this.getProjectTitle(branchRefName);
    }

    private static getProjectTitle(url: string): string {
        const words = url.split('/');
        return words[words.length - 1];
    }
}
