import * as Git from 'nodegit';
import FolderService from './folderService';
import NamingService from './namingService';
import AuthError from '../model/AuthError';
import UnknownError from '../model/UnknownError';
import AlreadyExistError from '../model/AlreadyExistError';

export default class GitService {
    // =======================================================================================
    // The GitService is the main accesspoint into the local provided repository or for the
    // one to be downloaded. The extraction of information out of the repos is made via a
    // recursive function chaining n operations to one big call. This design choice is made
    // due to the way nodegit, the core of the repository manipulation is working.
    // =======================================================================================
    // Rules for the defined Methods:
    //      - Function must be async
    //      - Function must take just one parameter
    //      - Function must return a Promise with just one parameter
    //
    // All Functions will be chained after the init call of opening the repo is done!
    // =======================================================================================

    public async gitCreationExtraction(infoPackage, callList) {

        const location = FolderService.REPO_FOLDER_SUB + infoPackage.repoName;

        const finalValue = await Git.Clone(infoPackage.repoUrl, location,
            this.createOptions(infoPackage.username, infoPackage.password))
            .then(
                (repo) => {
                    // The Repository is needed in many other calls, so we sore it too
                    infoPackage.repository = repo;
                    return this.chainNextCall(callList, 0, repo, infoPackage);
                })
            .catch((err) => {
                if (err.errno === -1) {
                    // Not Authorized
                    return new AuthError(err.toString());
                }
                else if (err.errno === -4) {
                    // Already existing
                    return new AlreadyExistError(err.toString());
                } else {
                    // Unknown
                    console.log(err);
                    return new UnknownError(err.toString());
                }
            });

        return finalValue;
    }

    public async gitInfoExtraction(infoPackage, callList) {

        const location = FolderService.REPO_FOLDER_SUB + infoPackage.repoName;

        const finalValue = await Git.Repository.open(location)
            .then(
                (repo) => {
                    // The Repository is needed in many other calls, so we sore it too
                    infoPackage.repository = repo;
                    return this.chainNextCall(callList, 0, repo, infoPackage);
                })
            .catch((err) => {
                // Unknown
                console.log(err);
                return new UnknownError(err.toString());
            });

        return finalValue;
    }

    // The recursive function and core of the whole mess
    public async chainNextCall(allCalls, index, parameter, infoPackage) {
        // If the end of the calls List is reached, return the value of the final method
        if (allCalls.length === index + 1) {
            return allCalls[index](parameter, infoPackage);
        }
        // Else there are still calls to chain, then previous call is therefor chained to the upcoming one
        else {
            return allCalls[index](parameter, infoPackage)
                .then(
                    (newParameter) => this.chainNextCall(allCalls, index + 1, newParameter, infoPackage),
                ).catch((err) => {
                    console.log(err);
                });
        }
    }

    // ====================================================
    // extraction Methods
    // ====================================================

    // (repo) => {return repo.getReferences(Git.Reference.TYPE.OID);}
    public async extractBranchHeadsfromRepo(repo, infoPackage) {
        return repo.getReferences(Git.Reference.TYPE.LISTALL);
    }

    public async extractRepofromRepoAndFetch(repo, infoPackage) {
        await repo.fetchAll(GitService.createFetchOptions(infoPackage.username, infoPackage.password));

        return repo;
    }

    public async extractIndexFromRepoAndRefresh(repo, infoPackage) {
        return repo.refreshIndex();
    }

    public async extractIndexFromIndexAndAddFile(index, infoPackage) {
        // just one file you dum dums
        await infoPackage.repository.checkoutBranch('refs/heads/' + NamingService.getBranchName(infoPackage.branch));
        await index.updateAll();
        await index.addByPath(FolderService.buildRepoInternPath(infoPackage));

        console.log('===============================================');
        console.log('number of files: ', index.entryCount());
        console.log('conflicts ', index.hasConflicts());
        for (const entry of index.entries()) {
            console.log('file: ', Git.Index.entryStage(entry), entry.path);
        }
        console.log('===============================================\n\n');

        return index;
    }

    public async extractIndexFromIndexAndWrite(index, infoPackage) {
        await index.write();
        return index;
    }

    public async extractTreeIdFromIndexAndWriteTree(index, infoPackage) {
        return index.writeTree();
    }

    public async extractIdHistoryFromRepo(repo, infoPackage) {

        let hasNext = true;
        const idHistory: any = [];
        const revwalk = Git.Revwalk.create(repo);

        revwalk.reset();
        revwalk.sorting(Git.Revwalk.SORT.TIME);
        revwalk.pushGlob('refs/heads/*');

        while (hasNext) {
            try {
                await revwalk.next().then((id) => {
                    idHistory.push(id);
                });
            } catch (err) {
                hasNext = false;
            }
        }
        revwalk.free();

        return idHistory;
    }

    public async extractCommitHistoryFromIdHistory(idHistory, infoPackage) {
        const history: any = [];

        for (const id of idHistory) {
            await infoPackage.repository.getCommit(id).then((commit) => {
                history.push({
                    id: commit.id().tostrS(),
                    author: commit.author().toString(),
                    committer: commit.committer().toString(),
                    date: commit.date(),
                    message: commit.message(),
                    parentNo: commit.parentcount(),
                    parents: commit.parents().map((oid) => oid.tostrS()),
                    summary: commit.summary(),
                    time: commit.time(),
                    string: commit.toString(),
                });
            });

        }

        return history;
    }

    public async extractBranchInfofromBranchHeads(references, infoPackage) {
        const branches: any = [];

        for (const ref of references) {
            if (ref.isRemote()) {
                branches.push({
                    target: ref.target().tostrS(),
                    short: NamingService.generateFolderName(ref.shorthand()),
                    name: ref.name(),
                });
            }
        }

        return branches;
    }

    // create local branches for each remote branch
    public async doUpdateBranchesfromBranchHeads(references, infoPackage) {
        const remoteOverflow = GitService.createBranchDiff(references);

        // create branch locally
        for (const branch of remoteOverflow) {
            await infoPackage.repository.createBranch(
                NamingService.generateFolderName(branch.name()), branch.target(),
            ).then((ref) => {
                console.log('Created:', ref.name());
            });
        }

        for (const branch of remoteOverflow) {
            await infoPackage.repository.checkoutRef(branch);
        }

        for (const ref of references) {
            await infoPackage.repository.mergeBranches(
                NamingService.generateFolderName(ref.name()),
                'origin/' + NamingService.generateFolderName(ref.name()),
            );

        }
    }

    public async doCommitFromTreeId(oid, infoPackage) {
        const remote = await infoPackage.repository.getRemote('origin');

        const message = 'GitAgent(template): added ' + infoPackage.file;
        // GitAgent is committer
        const committer = Git.Signature.now('GitAgent', infoPackage.username);
        // user is author
        const author = Git.Signature.now(infoPackage.username, infoPackage.username);
        // get target from current branch head
        const branch = await infoPackage.repository.getReference('refs/heads/' + NamingService.getBranchName(infoPackage.branch));
        const name = branch.name();
        // get head of the branch
        const parent = await infoPackage.repository.getCommit(branch.target());

        console.log('remote found :', !!remote, remote);
        console.log('message would be :', message);
        console.log('committer would be :', committer);
        console.log('author would be :', author);
        console.log('branch found :', !!branch, name);
        console.log('parent found :', !!parent, parent);
        console.log('parent is :', parent.message());

        await infoPackage.repository.createCommit(name, author, committer, message, oid, [parent])
            .then((oidNew) => {
                console.log('succsess', oidNew);
            }).catch((err) => {
                console.log(err);
            });

        remote.push(
            NamingService.getPushRefSpecs(infoPackage.branch),
            GitService.createFetchOptions(infoPackage.username, infoPackage.password),
            infoPackage.repository.defaultSignature(),
            'Push to ' + NamingService.getBranchName(infoPackage.branch),
        );
    }
    
    public async doCheckout(repo, infoPackage) {
        await infoPackage.repository.checkoutBranch('refs/heads/' + NamingService.getBranchName(infoPackage.branch));
    }

    // ====================================================
    // auth Methods
    // ====================================================

    public createOptions(username, password) {
        let attempts = 0;
        return {
            fetchOpts: {
                callbacks: {
                    credentials: () => {
                        if (attempts > 2) {
                            console.log('Cancel Auth after 3 attempts');
                            return Git.Cred.defaultNew();
                        }
                        attempts++;
                        console.log('In credential');
                        return Git.Cred.userpassPlaintextNew(username, password);
                    },
                },
            },
        };
    }

    public static createFetchOptions(username, password) {
        let attempts = 0;
        return {
            callbacks: {
                credentials: () => {
                    if (attempts > 2) {
                        console.log('Cancel Auth after 3 attempts');
                        return Git.Cred.defaultNew();
                    }
                    attempts++;
                    console.log('In credential');
                    return Git.Cred.userpassPlaintextNew(username, password);
                },
            },
        };
    }

    // ====================================================
    // helping Methods
    // ====================================================

    public static createBranchDiff(branches) {
        const headList: any = [];
        const remoteList: any = [];
        let remoteObjects: any = [];

        // collect remote and heads in lists (only last names)
        for (const ref of branches) {
            if (ref.name().indexOf('heads') >= 0) {
                headList.push(NamingService.generateFolderName(ref.name()));
            } else if (ref.name().indexOf('remotes') >= 0) {
                remoteList.push(NamingService.generateFolderName(ref.name()));
                remoteObjects.push(ref);
            }
        }

        // remove all exisitng branches from remote list
        for (const headEntry of headList) {
            if (remoteList.indexOf(headEntry) >= 0) {
                remoteList.splice(remoteList.indexOf(headEntry), 1);
                // yay filter
                remoteObjects = remoteObjects.filter((branch) => !branch.name().endsWith(headEntry));
                // remoteObjects.splice(remoteList.indexOf(headEntry), 1);
            }
        }

        return remoteObjects;
    }

    // ====================================================
    // Commit History Parsing
    // ====================================================

    // sorts commits into branches
    public createFormatedCommitHistory(branchList, commitHistory) {
        const totalCommits = commitHistory.length;
        let history: any = [];

        // Combine branch object with empty commit list
        for (const branch of branchList) {
            history.push({
                branchInfo: branch, // complete object
                commits: [],
            });
        }

        // TODO check if nedded
        // sort by master if exists
        history.sort((a, b) => {
            if (b.branchInfo.short.toLowerCase().endsWith('master')) return 1;
            if (a.branchInfo.short.toLowerCase().endsWith('master')) return 1;
        });

        // first fill in target commit
        for (const branch of history) {
            const target = branch.branchInfo.target;
            let commitIndex;

            // search for target an save index when found
            for (const commit of commitHistory) {
                if (commit.id === target) {
                    commitIndex = commitHistory.indexOf(commit);
                }
            }

            // remove target commit from plain Commit Hsitory and add to branchs commit list
            const extracted = commitHistory.splice(commitIndex, 1);
            branch.commits.push(extracted[0]);
        }

        // sort commits into branches by parent reference
        history = this.branchFilter(commitHistory, history);

        // fill commits list to have equal size
        history = this.fillHistory(history, totalCommits);

        // TODO sorting non named branches into existing for more readability

        // sorting branch order, master always first, then the more commits are in a branch
        history.sort((a, b) => {
            if (!a.branchInfo.short) {
                return 1;
            } else if (!b.branchInfo.short) {
                return -1;
            }

            if (a.branchInfo.short.toLowerCase().endsWith('master')) {
                return -1;
            } else if (b.branchInfo.short.toLowerCase().endsWith('master')) {
                return 1;
            } else {
                return b.commits.length - a.commits.length;
            }
        });

        return history;
    }

    private branchFilter(commitHistory, history) {

        // work through whole commitHistory untill its empty
        while (commitHistory.length > 0) {
            let commit = commitHistory.splice(0, 1)[0];

            // search branches with merges first
            for (const branch of history) {
                if (!branch.commits[branch.commits.length - 1]) continue;

                const lastCommitOfBranch = branch.commits[branch.commits.length - 1];

                if (lastCommitOfBranch.parents.indexOf(commit.id) >= 0) {
                    if (lastCommitOfBranch.parents.length > 1) {
                        if (this.allOtherIdsAreSortedIn(history, lastCommitOfBranch.parents, commit.id)) {
                            branch.commits.push(commit);
                            commit = null;
                            break;
                        }
                    }
                }
            }

            // search branches with normal commits later
            if (commit) {
                for (const branch of history) {
                    if (!branch.commits[branch.commits.length - 1]) continue;

                    const lastCommitOfBranch = branch.commits[branch.commits.length - 1];

                    if (lastCommitOfBranch.parents.indexOf(commit.id) >= 0) {
                        if (lastCommitOfBranch.parents.length === 1) {
                            branch.commits.push(commit);
                            commit = null;
                            break;
                        }
                    }
                }
            }

            // for the rare case that a commit is not pasted in a branch
            // aka. the branch was already deleted, push it into a hollow branch
            if (commit) {
                history.push({
                    branchInfo: {short: ''},
                    commits: [commit],
                });
            }
        }

        return history;
    }

    // Check if the all other commits exept the currret are taken
    private allOtherIdsAreSortedIn(history, parentIds, currentId) {
        const allTheOther = parentIds.filter((str) => str !== currentId);

        for (const idToCheck of allTheOther) {
            let numberFound = false;
            // Check if the id is in one of the branches commits
            for (const branch of history) {
                for (const commit of branch.commits) {
                    if (idToCheck === commit.id) {
                        numberFound = true;
                    }
                }
            }
            // if the current id is not found, not all of the ids are taken yet
            if (!numberFound) {
                return false;
            }
        }

        // When all commit ids are found put the next commit under the merge
        return true;
    }

    private fillHistory(history, totalCommits) {

        let maxCommitCount = 0;
        for (const branch of history) {
            if (branch.commits.length > maxCommitCount) {
                maxCommitCount = branch.commits.length;
            }
        }

        for (let i = 0; i < totalCommits; i++) {
            let latestCommitedBranchIndex = 0;
            let latestCommitedBranchTime = 0;
            for (let j = 0; j < history.length; j++) {
                if (history[j].commits.length <= i) continue;
                if (history[j].commits[i].time > latestCommitedBranchTime) {
                    latestCommitedBranchIndex = j;
                    latestCommitedBranchTime = history[j].commits[i].time;
                }
            }

            for (let k = 0; k < history.length; k++) {
                if (k !== latestCommitedBranchIndex) {
                    history[k].commits.splice(i, 0, undefined);
                }
            }
        }

        return history;
    }
}
