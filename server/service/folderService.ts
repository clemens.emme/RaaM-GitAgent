import * as fs from 'fs';
import * as path from 'path';

export default class FolderService {
    public static ASSET_FOLDER = '\\assets';
    public static REPO_FOLDER_NAME = 'gitRepos';
    public static REPO_FOLDER = '/' + FolderService.REPO_FOLDER_NAME;
    public static REPO_FOLDER_DOT = '.' + FolderService.REPO_FOLDER;
    public static REPO_FOLDER_SUB = FolderService.REPO_FOLDER_DOT + '/';

    public static getDefaultPath() {
        return path.dirname(require.main.filename);
    }

    public static getProjectPath() {
        return FolderService.getDefaultPath().substring(0, FolderService.getDefaultPath().indexOf('Raam-GitAgent') + 'Raam-GitAgent'.length);
    }

    public static buildAssetPath(infoPackage) {
        const assetPath = FolderService.getProjectPath() + FolderService.ASSET_FOLDER + '\\' + infoPackage.file;
        return assetPath.replace(/\//g, '\\');
    }

    public static buildDestPath(infoPackage) {
        const destinationPath = FolderService.getProjectPath() + '\\' + FolderService.REPO_FOLDER_NAME + '\\' + infoPackage.projectName + '\\' + infoPackage.path;
        return destinationPath.replace(/\//g, '\\');
    }

    public static buildRepoInternPath(infoPackage) {
        return infoPackage.path;
    }

    public getLocalRepoList() {
        const repoList: any[] = [];
        const files = fs.readdirSync(FolderService.REPO_FOLDER_SUB);

        files.forEach((file) => {
            if (fs.statSync(FolderService.REPO_FOLDER_SUB + file).isDirectory()) {
                repoList.push(file.toString());
            }
        });

        return repoList;
    }

    public getRepoFolderSystem(repoName) {
        const result = this.readFileSystem(FolderService.REPO_FOLDER_SUB + repoName + '/');
        return result;
    }

    public readFileSystem(dir) {
        const result: any = [];
        const list = fs.readdirSync(dir);

        list.forEach((file) => {
            if (!file.toString().startsWith('.')) {
                if (fs.statSync(dir + file).isDirectory()) {
                    result.push({
                        name: file.toString(),
                        type: 'dir',
                        sub: this.readFileSystem(dir + file + '/'),
                    });

                } else {
                    result.push({
                        name: file.toString(),
                        type: 'file',
                    });
                }
            }
        });

        return result;
    }

    public writeFileToRepo(infoPackage) {

        const assetPath = FolderService.buildAssetPath(infoPackage);
        const destinationPath = FolderService.buildDestPath(infoPackage);
        console.log('Move from', assetPath);
        console.log('to', destinationPath);
        this.copyFile(assetPath, destinationPath);
        this.deleteFileFromAsset(assetPath);
    }

    private deleteFileFromAsset(assetPath: string) {
        fs.unlinkSync(assetPath);
    };

    private copyFile(src, dest) {
        const readStream = fs.createReadStream(src);

        readStream.once('error', (err) => {
            console.log(err);
        });

        readStream.once('end', () => {
            console.log('done copying');
        });

        readStream.pipe(fs.createWriteStream(dest));
    }
}
