import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as log from 'fancy-log';
import * as helmet from 'helmet';
import * as morgan from 'morgan';
import * as multer from 'multer';

import setRoutes from './routes';

const port = '7070';
const app = express();
const storage = multer.diskStorage({
    destination: './assets/',
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    },
});

/**
 * Configure server
 */

// middleware
app.use(multer({
    storage: storage,
}).any());
app.use(morgan('dev'));
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors());
app.options('*', cors());

setRoutes(app);

/**
 * Start the server
 */

app.listen(port, () => {
    log.info(`Service running on http://localhost:` + port + '\n');
    log.info(`Service running in ${app.get('env')} mode\n`);
});

module.exports = app;
