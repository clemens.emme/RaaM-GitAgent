import UError from './UnderstandAbleError';

export default class UnknownError extends UError {

    public type = 'UnkownError';
    public description = 'Fehler bislang unbekannt';

}
