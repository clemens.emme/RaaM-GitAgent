import UError from './UnderstandAbleError';

export default class AlreadyExistError extends UError {

    public type = 'AlreadyExistError';
    public description = 'Das angeforderte Repository ist bereits geklont worden';

}
