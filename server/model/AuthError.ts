import UError from './UnderstandAbleError';

export default class AuthError extends UError {

    public type = 'AuthError';
    public description =
        'Fehler beim Zugriff auf das Repository.\n' +
        'Das Repository ist privat oder die eingegebenen Anmeldedaten sind fehlerhaft.';

}
