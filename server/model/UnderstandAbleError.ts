abstract class UError {

    public abstract type;
    public abstract description;
    public originalError;

    constructor(givenError) {
        this.originalError = givenError;
    }
}

export default UError;