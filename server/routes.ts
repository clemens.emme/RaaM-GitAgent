import * as express from 'express';
import * as multer from 'multer';
import GitCloneCtrl from './controllers/gitCloneController';

export default function setRoutes(app) {

    const router = express.Router();
    const upload = multer({dest: 'assets/'});
    const cloneCtrl = new GitCloneCtrl();

    // util routes
    const baseRoute = '';
    const addRoute = '/add';

    // Repository Info routes
    const repoNameRoute = '/repo/:name'; // repo text part is needed so the app wont read /add as a /:name
    const repoNameHistoryRoute = repoNameRoute + '/history';
    const repoNameBrancheRoute = repoNameRoute + '/branch';
    const repoNameFileSystemROute = repoNameRoute + '/files';

    // Repository Commit route
    const repoCommitRoute = repoNameRoute + '/commit';

    // ==================================================================================================================================
    // File Upload
    // ==================================================================================================================================
    router.route(addRoute).post(upload.single('file'), cloneCtrl.uploadFile);

    // ==================================================================================================================================
    // Repo
    // ==================================================================================================================================
    router.route(baseRoute).get(cloneCtrl.getAllReposNameList); // getAll
    router.route(baseRoute).post(cloneCtrl.cloneOneRepo); // cloneOneRepo

    router.route(repoNameRoute).delete(cloneCtrl.deleteOneRepo); //  removeFileAndCommit

    // These are all put calls because of the fetch that is performed in them
    // Fetching needs username and password
    router.route(repoNameHistoryRoute).put(cloneCtrl.getOneReposCommitHistory); // getOneCommitHistory
    router.route(repoNameBrancheRoute).put(cloneCtrl.getOneReposBranchList);  // getOneBranchList
    router.route(repoNameFileSystemROute).put(cloneCtrl.getOneReposFileSystem); // getOneFileSystem

    // ==================================================================================================================================
    // Commit
    // ==================================================================================================================================
    router.route(repoCommitRoute).post(cloneCtrl.addFileAndCommit); // addFileAndCommit
    router.route(repoCommitRoute).delete(cloneCtrl.removeFileAndCommit); //  removeFileAndCommit

    app.use('/api', router);
}
