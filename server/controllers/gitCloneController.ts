import NamingService from '../service/namingService';
import FolderService from '../service/folderService';
import GitService from '../service/gitService';

export default class GitCloneCtrl {

    private folderService = new FolderService();
    private gitService = new GitService();
    private fetchTimer = true;
    private waitingTime = 60000;

    // ==================================================================================================================================
    // Helper Methods fore reusing
    // ==================================================================================================================================

    public resetTimer() {
        this.fetchTimer = true;
    }

    public setTimer() {
        this.fetchTimer = false;
        setTimeout(() => this.resetTimer(), this.waitingTime);
    }

    public async performFetch(infoPackage) {
        if (infoPackage.password) {
            if (this.fetchTimer) {
                // Fetch all and then merge
                await this.gitService.gitInfoExtraction(infoPackage, [
                    this.gitService.extractRepofromRepoAndFetch,
                    this.gitService.extractBranchHeadsfromRepo,
                    this.gitService.doUpdateBranchesfromBranchHeads,
                ]);
                this.setTimer();
            } else {
                console.log('timer still Ticking, no auth');
            }
        }
    }

    public async performCheckout(infoPackage) {
        if (infoPackage.branch) {
            await this.gitService.gitInfoExtraction(infoPackage, [
                this.gitService.doCheckout,
            ]);
        }
    }

    public async getBranchList(infoPackage) {
        const branchList = await this.gitService.gitInfoExtraction(infoPackage, [
            this.gitService.extractBranchHeadsfromRepo,
            this.gitService.extractBranchInfofromBranchHeads,
        ]);

        return branchList;
    }

    public async getCommitHistory(infoPackage) {
        const commitHistory = await this.gitService.gitInfoExtraction(infoPackage, [
            this.gitService.extractIdHistoryFromRepo,
            this.gitService.extractCommitHistoryFromIdHistory,
        ]);

        return commitHistory;
    }

    public parseHistory(branchList, commitHistory) {
        return this.gitService.createFormatedCommitHistory(branchList, commitHistory);
    }

    // ==================================================================================================================================
    // Repo
    // ==================================================================================================================================

    // Get All Cloned Repo Names
    public getAllReposNameList = (req, res) => {
        const repoList = this.folderService.getLocalRepoList();
        res.status(200).json(repoList);
    };

    // ==================================================================================================================================
    // History
    // ==================================================================================================================================

    // Formatted Git History
    public getOneReposCommitHistory = async (req, res) => {
        const infoPackage = {
            repoUrl: req.body.gitUrl,
            repoName: req.params.name,
            username: req.body.username,
            password: req.body.password,
        };

        // Skip if not user Data
        await this.performFetch(infoPackage);

        // build new commit history
        const commitHistory = await this.getCommitHistory(infoPackage);

        // build new branch list
        const branchList = await this.getBranchList(infoPackage);

        // combine branch list and commit history
        const history = await this.parseHistory(branchList, commitHistory);

        res.status(200).json(history);
    };

    // ==================================================================================================================================
    // Branches
    // ==================================================================================================================================

    public getOneReposBranchList = async (req, res) => {
        const infoPackage = {
            repoUrl: req.body.gitUrl,
            repoName: req.params.name,
            username: req.body.username,
            password: req.body.password,
        };

        // Skip if not user Data
        await this.performFetch(infoPackage);

        // build new branch list
        const branchList = await this.getBranchList(infoPackage);

        res.status(200).json(branchList);
    };

    // ==================================================================================================================================
    // File System
    // ==================================================================================================================================

    public getOneReposFileSystem = async (req, res) => {
        const infoPackage = {
            branch: req.body.branch,
            repoUrl: req.body.gitUrl,
            repoName: req.params.name,
            username: req.body.username,
            password: req.body.password,
        };

        // Skip if not user Data
        await this.performFetch(infoPackage);

        // Checkout Branch if needed
        await this.performCheckout(infoPackage);

        const repoList = this.folderService.getRepoFolderSystem(infoPackage.repoName);
        res.status(200).json(repoList);
    };

    // ==================================================================================================================================
    // Clone
    // ==================================================================================================================================

    public cloneOneRepo = async (req, res) => {
        const infoPackage = {
            repoUrl: req.body.gitUrl,
            repoName: NamingService.generateFolderName(req.body.gitUrl),
            username: req.body.username,
            password: req.body.password,
        };

        // clone Repo
        await this.gitService.gitCreationExtraction(infoPackage, [
            this.gitService.extractIdHistoryFromRepo, // TODO remove History Building
            this.gitService.extractCommitHistoryFromIdHistory, // TODO remove History Building
        ]);

        res.status(200).json(infoPackage.repoName);
    };

    // ==================================================================================================================================
    // Commit
    // ==================================================================================================================================
    //
    //   Request Data :
    //   --------------
    //   path:     string
    //   branch:    string
    //   gitUrl:    string
    //   fileName:  string
    //   username:   string
    //   password:   string
    //   projectName: string

    public addFileAndCommit = async (req, res) => {
        const infoPackage = {
            file: req.body.fileName,
            path: req.body.path,
            branch: req.body.branch,
            commit: req.body.commit,
            gitUrl: req.body.gitUrl,
            username: req.body.username,
            password: req.body.password,
            projectName: req.body.projectName,
            repoName: NamingService.generateFolderName(req.body.gitUrl),
        };

        // reset Timer to force fetch
        this.resetTimer();

        // Skip if not user Data
        await this.performFetch(infoPackage);

        // TODO Later
        // search if branchName is in List, if not create

        // Checkout branch
        await this.performCheckout(infoPackage);

        // copy file to the destination folder, if needed
        await this.folderService.writeFileToRepo(infoPackage);

        // make a commit on branch and push
        await this.gitService.gitInfoExtraction(infoPackage, [
            // refresh index
            this.gitService.extractIndexFromRepoAndRefresh,
            // add file to index
            this.gitService.extractIndexFromIndexAndAddFile,
            // write index
            this.gitService.extractIndexFromIndexAndWrite,
            // writeTree index
            this.gitService.extractTreeIdFromIndexAndWriteTree,
            // commit
            this.gitService.doCommitFromTreeId,
        ]);

        // TODO Propper return value
        res.status(200).json({});
    };

    // ==================================================================================================================================
    // File Upload
    // ==================================================================================================================================

    public uploadFile = (req, res) => {
        res.status(200).json({});
    };

    // ==================================================================================================================================
    // TODO
    // ==================================================================================================================================

    public removeFileAndCommit = (req, res) => {
        // TODO when called, remove a file and commit
        res.status(200).json({});
    };

    public deleteOneRepo = (req, res) => {
        // TODO delete one by project name, using rimraf
        res.status(200).json({});
    };
}
