FROM jfyne/node-alpine-yarn:latest
COPY . .
RUN yarn install --frozen-lockfile && yarn build

FROM jfyne/node-alpine-yarn:latest
ENV NODE_ENV=production
COPY --from=0 /usr/src/app/build .
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile
EXPOSE 3000
CMD ["node", "app"]
