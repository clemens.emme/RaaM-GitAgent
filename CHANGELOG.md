<a name="1.0.0"></a>
# 1.0.0 (2018-01-16)


### Bug Fixes

* refactor all the things, fix linting ([027bbdc](https://gitlab.com/mdbda/flow-meta/commit/027bbdc))
